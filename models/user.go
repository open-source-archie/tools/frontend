package models

import (
	"time"
)

type User struct {
	FirstName  string
	LastName   string
	Email      string
	SignupTime time.Time
}
