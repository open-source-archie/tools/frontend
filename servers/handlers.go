package servers

import (
	"gitlab.com/open-source-archie/tools/frontend/models"
	"html/template"
	"net/http"
)

func (s *server) indexHandler(w http.ResponseWriter, r *http.Request)  {
	toolsList := []models.Tools{
		{
			Name: "Image Metadata Decoder",
			URL:  "/imgdecode",
			Description: "Returns the EXIF metadata for any file uploaded",
		},
		{
			Name: "Audio MetaData Decoder",
			URL: "/audiodecode",
			Description: "Returns the audio metadata for any file uploaded",
		},
		{
			Name: "Youtube Video Downloader",
			URL: "/ytdownload",
			Description: "Downloads any video from a YouTube URL",
		},

	}

	templ, _ := template.ParseFiles(
		"templates/index.html",
		"templates/header.html",
		"templates/footer.html",
	)
	err := templ.Execute(w, toolsList)
	if err != nil {
		s.logger.Error(err.Error())
	}
}
