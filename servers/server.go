package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/open-source-archie/tools/frontend/config"
	"gitlab.com/open-source-archie/tools/frontend/repositories"

	"go.uber.org/zap"
	"net/http"
)

type server struct {
	http.Server
	router     *mux.Router
	logger     *zap.Logger
	repo       repositories.UserPoster
}

func NewServer(
	logger *zap.Logger,
	cfg *config.Server,
	repo repositories.UserPoster,
) (s *server) {
	s = &server{
		router:     mux.NewRouter(),
		logger:     logger,
		repo:       repo,
	}

	s.Server = http.Server{
		Addr:    fmt.Sprintf(":%d", cfg.Port),
		Handler: s,
	}
	s.registerRoutes()
	return
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) Run() {
	// Todo: Handle graceful shutdown with channel pattern!
	s.logger.Info(fmt.Sprintf("running on port %s", s.Addr))
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		s.logger.Error(err.Error())
	}
}
