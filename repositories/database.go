package repositories

import (
	"database/sql"
	"go.uber.org/zap"
)

type UserPoster interface {

}

// repository is a custom type which wraps the sql.DB connection pool REPO
type repository struct {
	logger *zap.Logger
	DB     *sql.DB
}

func NewRepository(logger *zap.Logger, db *sql.DB) *repository {
	return &repository{
		logger: logger,
		DB:     db,
	}
}
