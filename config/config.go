package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Repository Repository `yaml:"DATABASE"`
	Server     Server     `yaml:"SERVER"`
	GRPCClient GRPCClient `yaml:"GRPCCLIENT"`
}

type Repository struct {
	Name     string `yaml:"DB_NAME"`
	User     string `yaml:"DB_USER"`
	Password string `yaml:"DB_PASSWORD"`
	Host     string `yaml:"DB_HOST"`
	Port     int    `yaml:"DB_PORT"`
	Schema   string `yaml:"DB_SCHEMA"`
}

type Server struct {
	Port int `yaml:"PORT"`
}

type GRPCClient struct {
	Port int `yaml:"PORT"`
}

// GetConfig loads the variables from config.ini
func GetConfig() (*Config, error) {
	f, err := os.Open("./config.yml")
	if err != nil {
		// Todo: handle error
		return nil, err
	}
	defer f.Close()
	// Todo: handle error

	var cfg Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		// Todo: handle error
		return nil, err
	}

	return &cfg, nil
}
